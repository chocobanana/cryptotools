<?php

namespace Chocobanana\Cryptotools\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class EncodePasswordCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('encode:password')
            ->setDescription('Encode a password with PBKDF2 to make it stronger and safer. You can set the parameters in the command and have them written into a file, or read them directly from a file.')
            ->addArgument(
                'read-or-write',
                InputArgument::REQUIRED,
                'Whether to read from the file or write into the file.'
            )
            ->addArgument(
                'file',
                InputArgument::REQUIRED,
                'Path to the file where the data required to rehash the password will be stored.'
            )
            ->addOption(
                'salt',
                null,
                InputOption::VALUE_REQUIRED,
                'Salt?'
            )
            ->addOption(
                'iterations',
                null,
                InputOption::VALUE_REQUIRED,
                'How many iterations?'
            )
            ->addOption(
                'algorithm',
                null,
                InputOption::VALUE_REQUIRED,
                'Algorithm?'
            )
            ->addOption(
                'length',
                null,
                InputOption::VALUE_REQUIRED,
                'Length desired for the hash?'
            )
            ->addOption(
                'debug',
                null,
                InputOption::VALUE_NONE,
                'Debug?'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $debug = $input->getOption('debug');
        $read_or_write = $input->getArgument('read-or-write');
        if ($read_or_write == 'write') {
            $salt = $input->getOption('salt');
            if ( ! $salt) {
                $salt = md5(openssl_random_pseudo_bytes(40, $crypto_strong));
                if ( ! $crypto_strong) {
                    throw new \RuntimeException("Pseudo random number generation failed.");
                }
            }
            $iterations = $input->getOption('iterations');
            $algorithm = $input->getOption('algorithm');
            $algorithm or $algorithm = 'sha256';
            $length = $input->getOption('length');
            $length or $length = 40;
            $file = $input->getArgument('file');
            $password = $this->getPassword($output);

            $hash = hash_pbkdf2($algorithm, $password, $salt, $iterations, $length);

            $json_data = json_encode(array(
                'salt' => $salt,
                'algorithm' => $algorithm,
                'iterations' => $iterations,
                'length' => $length,
            ));

            $success = file_put_contents($file, $json_data);

            if ( ! $success) {
                throw new \RuntimeException("Failed to write \$data to '{$file}'.");
            }
        } else if ($read_or_write == 'read') {
            $file = $input->getArgument('file');
            $json_data = file_get_contents($file);
            $data = json_decode($json_data, true);
            extract($data);
            $password = $this->getPassword($output, false);
            
            $hash = hash_pbkdf2($algorithm, $password, $salt, $iterations, $length);
        } else {
            throw new \InvalidArgumentException("Invalid argument for read_or_write: '{$read_or_write}'.");
        }
        
        if ($debug) {
            $output->writeln($json_data);
            $output->writeln("Hash: {$hash}");
        } else {
            $output->writeln($hash);
        }
    }
    
    private function getPassword(OutputInterface $output, $with_confirmation = true)
    {
        $dialog = $this->getHelperSet()->get('dialog');
        $password = $dialog->askHiddenResponse(
            $output,
            'Password:',
            false
        );
        if ($with_confirmation) {
            $password_confirmation = $dialog->askHiddenResponse(
                $output,
                'Again:',
                false
            );
            if ($password != $password_confirmation) {
                throw new \RuntimeException("Passwords don't match.");
            }
        }
        return $password;
    }
}